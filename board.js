const makeCheckerBoard = () => {
    let changing = true;
    let str = '';

    for (let i = 0; i < 8; i++) {
        for (let j =0; j < 8; j++) {
            const even = j % 2 === 0;

            if (changing) {
                str += even ? '  ' : '██';
            } else {
                str += even ? '██' : '  ';
            }
        }
        str += '\n';
        changing = !changing;
    }
    console.log(str);
};

makeCheckerBoard();